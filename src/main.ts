import Vue from 'vue';
import Vuetify from 'vuetify';
import { ValidationProvider } from 'vee-validate';
import { extend } from 'vee-validate';
import { required, email } from 'vee-validate/dist/rules';
import App from './App.vue';
import router from './router';
import store from './store';

import './registerServiceWorker';
import 'vuetify/dist/vuetify.min.css';

// Add the required rule
extend('required', required);

// Add the email rule
extend('email', email);

Vue.component('ValidationProvider', ValidationProvider);

Vue.config.productionTip = false;

Vue.use(Vuetify);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
