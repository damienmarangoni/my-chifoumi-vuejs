import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    winner: ''
  },
  mutations: {
    updateWinner(state, winner) {
      Vue.set(state, 'winner', winner);
    },
  },
  actions: {
    winner({commit}, value) {
      commit('updateWinner', value);
    }
  },
});
