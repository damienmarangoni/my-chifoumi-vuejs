/* eslint-disable */
export default {
  name: 'user',
  components: {},
  props: [
    'firstName',
    'lastName',
    'avatar'
  ],
  data () {
    return {
    }
  },
  computed: {

  },
  mounted () {

  },
  methods: {

  }
}
