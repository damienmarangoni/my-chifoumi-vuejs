/* eslint-disable */
import User from '@/components/User/User.vue'
import UserServices from '@/services/UserServices'

export default {
  name: 'users-list',
  components: {
    User
  },
  props: [],
  data () {
    return {
      users: [],
      nameFilter: ''
    }
  },
  computed: {

    usersToFilter: function() {
      if (!!this.nameFilter) {

        return this.users.filter(user => user.first_name.includes(this.nameFilter) ||
                                         user.last_name.includes(this.nameFilter));
      } else {

        return this.users;
      }
    }

  },
  mounted () {
    UserServices.getUsersList()
    .then(response =>  {
      this.users = response.data.data
    });
  },
  methods: {

  }
}
