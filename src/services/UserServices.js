/* eslint-disable */
import axios from 'axios'

export default {

  getUsersList() {
    return axios.get('https://reqres.in/api/users')
  }
}
